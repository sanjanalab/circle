# CIRCLE: A Robust Predictor of Immunotherapy Response Utilizing Whole-Exome Sequencing

This repository contains software for the design and analysis of the Cancer Immunotherapy Response CLassifiRr (CIRCLE) that predicts immunotherapy response from whole exome sequencing data. This repository is intended to accompany our manuscript. 

For more information please refer to:

A Robust Predictor of Immunotherapy Response Using Whole Exome Sequencing (2022)

Zoran Gajic, Aditya Deshpande, Mateusz Legut, Marcin Imielinksi*, Neville E. Sanjana*

For any questions about the code repository or Jupyter notebook, please contact Zoran: zgajic@nygenome.org

## Overview:

Here we present a Jupyter Notebook that recreates analyses, figures and values reported in our publication. This code was built on Linux but should be compatible with macOS and windows with minor modifications to paths. If you find our analyses useful in your work, we would be grateful if you could cite the publication above.


## Getting up and running:

### To run our analyses you will need the following dependencies: 

Jupyter Notebook with R 4.0.2 kernel installed (Later versions of R will likely work, however all of our testing was done in R 4.0.2). We expect the notebook to be robust across package versions, however the specific versions of all packages used can be found at the end of the manuscript & Jupyter notebook.

### Repository Structure

Code that is not available through CRAN or Bioconductor is stored in the 'scripts' directory, and some functions were loaded explicitly in the code.

Data that was generated prior to our analysis is stored in the 'DB' directory.

Data that is generated during our analysis is stored in the 'data' directory. This data is pre-cached but is overwritten when running the code.

Figures and other vizualizations including tables and supplementary data are pre-cached and stored in the 'viz' directory. When running the code, these figures will be overwritten with the newly generated figures.


### If you encounter any errors
Please feel free to raise a GitLab issue. If you need any help running these scripts or would like to provide feedback, please reach out to us at zgajic@nygenome.org, mimielinski@nygenome.org, and nsanjana@nygenome.org.


## To run the analysis:

1) Make sure Jupyter Notebook with R 4.0.2 is installed and active. This can be done by installing Anaconda (https://www.anaconda.com/) and following the tutorial here: https://docs.anaconda.com/anaconda/navigator/tutorials/r-lang/

2) Download the CIRCLE repository. Information on how to do that can be found here:https://docs.gitlab.com/ee/user/project/repository/

3) Extract the CIRCLE respository

4) Run Jupyter and open the 'CIRCLE_notebook.ipynb' notebook.

5) The notebook is divided into 'Analyses' which can be run in any order without needing to run earlier code. The code can also be run from top to bottom.

6) Values, figures, and tables reported in the publication will be re-created in the Jupyter notebook output. Figures and tables will also be stored in the 'viz' folder.

